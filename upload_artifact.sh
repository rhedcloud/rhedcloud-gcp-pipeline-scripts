#!/bin/bash

set -e

if [[ $# < 1 ]]; then
    >&2 cat <<EOT
Upload build artifacts to the current repository's Downloads section.

Usage:

    ${0} ARTIFACT...

Positional arguments:

    ARTIFACT  One or more artifacts to upload to the current repository's
              Downloads page.

For example:

    ${0} rhedcloud-gcp-rs-project-dm.latest.zip
    ${0} rhedcloud-gcp-rs-project-dm.latest.zip rhedcloud-gcp-rs-project-dm.latest.json

EOT
    exit 1
fi

for artifact in "${@}"; do
    echo "Uploading ${artifact}..."
    curl -SsfX POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"${artifact}"
done
