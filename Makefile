requirements:
	pip-compile requirements.in
	pip-compile requirements-dev.in

install-deps:
	pip-sync requirements.txt requirements-dev.txt
