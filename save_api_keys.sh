#!/bin/bash
#
# save_api_keys.sh: save GCP service account credentials to disk.
#
# This script assumes that one or more GCP service account API keys are
# base64-encoded as environment variables with a common prefix. It should
# iterate over all environment variables with this common prefix and save the
# decoded API key to a file on disk which can later be used for automated
# testing purposes.

# KEY_PREFIX is a prefix to use when filtering out variables we don't expect to
# hold base64-encoded API key information.
KEY_PREFIX="${KEY_PREFIX:-API_KEY}"

PERSIST_PATH="${PERSIST_PATH:-/tmp/key_file}"

# iterate over each environment variable
while IFS='=' read -r name value ; do
    # check whether the variable has the desired prefix
    if [[ "${name}" == "${KEY_PREFIX}"* ]]; then
        # if so, decode the variable's value and persist to disk
        target="${PERSIST_PATH}${name/${KEY_PREFIX}/}"

        echo "Saving ${name} to ${target}"
        echo ${value} | base64 -d > "${target}"
    fi
done < <(env)
