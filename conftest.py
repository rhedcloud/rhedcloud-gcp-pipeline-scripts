from contextlib import ExitStack
from functools import partial

import boto3
import pytest

from aws_test_functions.cleanup import cleanup
from aws_test_functions import (
    attach_policy,
    create_session_for_test_user,
    debug,
    new_test_user,
)


pytest_plugins = (
    "aws_test_functions.plugin.aws_accessdenied",
    "aws_test_functions.plugin.dict_fixture",
)


@pytest.fixture(scope='session', autouse=True)
def cleanup_resources():
    """Automatically clean up unused resources before running tests."""

    cleanup()


def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._prev_failed = item


def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        prev_failed = getattr(item.parent, '_prev_failed', None)
        if prev_failed is not None:
            pytest.xfail('previous test failed ({})'.format(prev_failed.name))


@pytest.fixture(scope="module")
def stack():
    with ExitStack() as stack:
        yield stack


@pytest.fixture(scope="module")
def user_policies():
    return (
        "RHEDcloudAdministratorRolePolicy",
        "AdministratorAccess",
    )


@pytest.fixture(scope="module")
def session(request, user_policies, stack):
    u = stack.enter_context(new_test_user(request.module.__name__))
    stack.enter_context(attach_policy(u, *user_policies))

    yield create_session_for_test_user(u.user_name)


@pytest.fixture
def factory(user_type, session):
    return boto3 if user_type == "admin" else session


@pytest.fixture(autouse=True)
def debug_user_type(request, mocker):
    """Prepend debug lines with the current user_type, if any."""

    if "user_type" not in request.fixturenames:
        return

    user_type = request.getfixturevalue("user_type")
    if user_type and "debug" in request.module.__dict__:
        mocker.patch.object(
            request.module,
            "debug",
            partial(debug, "[user_type={}]".format(user_type)),
        )


pytest.user_type = partial(pytest.mark.parametrize, "user_type")
pytest.access_denied = partial(pytest.param, marks=pytest.mark.raises_access_denied)
pytest.admin_only = pytest.user_type(["admin"])
pytest.user_only = pytest.user_type(["user"])
pytest.admin_and_user = pytest.user_type(["admin", "user"])
pytest.admin_and_blocked_user = pytest.user_type(["admin", pytest.access_denied("user")])
pytest.blocked_user_only = pytest.user_type([pytest.access_denied("user")])
