#!/bin/bash

set -e

repo="$1"
artifact="${2:-${repo}.latest.zip}"

if [[ "${1}" == "" ]]; then
    >&2 cat <<EOT
Download and extract build artifacts from other RHEDcloud repositories.

Usage:

    ${0} REPO [ARTIFACT]

Positional arguments:

    REPO      Name of the repository hosting the build artifact.
    ARTIFACT  Name of the build artifact to download from REPO's Downloads
              section. This is assumed to be a Zip file. If omitted, the
              default value is "\$REPO.latest.zip".

For example:

    ${0} rhedcloud-aws-python-testutils
    ${0} rhedcloud-aws-python-testutils rhedcloud-aws-python-testutils.latest.zip

EOT
    exit 1
fi

bb_team=${BB_TEAM:-itarch}
curl -LOSsf "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${bb_team}/${repo}/downloads/${artifact}"

if [[ "${artifact}" == *.zip ]]; then
    mkdir -p "${repo}"
    unzip "${artifact}" -d "${repo}"
fi
