#!/usr/bin/env python3

from googleapiclient.errors import HttpError
from googleapiclient import discovery
from oauth2client.client import GoogleCredentials
import time
import os
import sys

def delete_deployment(service, deployment, project_id):
    try:
        request = service.deployments().delete(deployment=deployment, project=project_id)
        response = request.execute()
        op_name = response['name']
        wait_for_operation(service, op_name, deployment, project_id)
    except HttpError as err:
       print("Error",err)

# [END delete_deployment]

def wait_for_operation(service, op_name, deployment, project_id):
    print('Waiting for operation to finish')
    while True:
        result = service.operations().get(operation=op_name, project=project_id).execute()
        if result['status'] == 'DONE':
            print("done")
            break
    if 'error' in result:
                #raise Exception(result['error'])
       request = service.deployments().delete(deployment=deployment, project=project_id, deletePolicy="ABANDON")
       response = request.execute()
       abandon_op = response['name']
       while True:
          abandon_result = service.operations().get(operation=abandon_op, project=project_id).execute()
          if abandon_result['status'] == 'DONE':
             print("abandon done")
             if 'error' in abandon_result:
                raise Exception(abandon_result['error'])
             break
    return result

    time.sleep(2)

# [END wait_for_operation]


def main():
    # uncomment below line if setting credentials via environment variable
    # credentials = GoogleCredentials.get_application_default()

    # set variables
    service = discovery.build('deploymentmanager', 'v2beta')
    project_id = os.getenv('PROJECT_ID')
    # deployment = os.getenv('DEPLOYMENT_NAME')
    deployment = sys.argv[1]

    delete_deployment(service, deployment, project_id)


if __name__ == '__main__':
    main()
