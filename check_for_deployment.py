#!/usr/bin/python


from googleapiclient import discovery
from oauth2client.client import GoogleCredentials

# credentials = GoogleCredentials.get_application_default()

service = discovery.build('deploymentmanager', 'v2beta')

project_id = 'gcp-project-1-257718'

request = service.deployments().get(deployment='rhedcloud-gcp-rs-project', project=project_id)
response = request.execute()

print(response)
if 'name' in response and response['name']=='rhedcloud-gcp-rs-project':
    request = service.deployments().delete(deployment='rhedcloud-gcp-rs-project', project=project_id)
    response = request.execute()
    #TODO make sure it worked
    print(response)

else:
    print('rhedcloud-gcp-rs-project does not exist')
